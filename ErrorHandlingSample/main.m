//
//  main.m
//  ErrorHandlingSample
//
//  Created by Tamás Zsoldos on 2016. 05. 12..
//  Copyright © 2016. zsthomas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
