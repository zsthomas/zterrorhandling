//
//  AppDelegate.h
//  ErrorHandlingSample
//
//  Created by Tamás Zsoldos on 2016. 05. 12..
//  Copyright © 2016. zsthomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

